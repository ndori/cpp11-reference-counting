test: test.o mystring.o
		g++ -g -o test test.o mystring.o 

test.o: test.cpp mystring.h 
		g++ -std=c++11 -Wall -Wdeprecated -pedantic -g -c test.cpp -o test.o

mystring.o: mystring.cpp mystring.h
		g++ -std=c++11 -Wall -Wdeprecated -pedantic -g -c mystring.cpp -o mystring.o


