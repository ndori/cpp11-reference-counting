#include <iostream>
#include <cstring>
#include <utility>

#include "mystring.h"

StringValue::StringValue(char const *init) {
	ref_counter_ = 1;
	len_ = strlen(init);
	data_= new char[len_+1];
	strcpy(data_, init);
	//std::cout << "string ctor" << std::endl;
}

StringValue::~StringValue() {
	delete[] data_;
	//std::cout << "strign dtor" << std::endl;
}

StringValue::StringValue(StringValue const &other) {
	ref_counter_ = 1;
	len_ = other.len_;
	data_ = new char[len_+1];
	strcpy(data_, other.data_);
	//std::cout << "strign copy ctor" << std::endl;
}

StringValue::StringValue(StringValue &&other) noexcept {
	ref_counter_ = 1;
	len_ = other.len_;
	other.len_ = 0;
	data_ = other.data_;
	other.data_ = nullptr;
	//std::cout << "string move ctor" << std::endl;
}

StringValue& StringValue::operator=(StringValue const &other) {
	if (this != &other) {
		delete[] data_;
		len_ = other.len_;
		data_ = new char[len_+1];
		strcpy(data_, other.data_);
	}
	//std::cout << "strign ertekado op" << std::endl;
	return *this;
}

StringValue& StringValue::operator=(StringValue &&other) noexcept {
	if (this != &other) {
		delete[] data_;
		len_ = other.len_;
		other.len_ = 0;
		data_ = other.data_;
		other.data_ = nullptr;
	}
	//std::cout << "move assignment" << std::endl;
	return *this;
}

char const& StringValue::operator[](size_t index) const {
	return data_[index];
}

char& StringValue::operator[](size_t index) {
	return data_[index];
}

StringValue StringValue::operator+(StringValue const &rhs) const {
	StringValue tmp;
	delete[] tmp.data_;
	tmp.len_ = this->len_ + rhs.len_;
	tmp.data_ = new char[tmp.len_ + 1];
	strcpy(tmp.data_, this->data_);
	strcat(tmp.data_, rhs.data_);
	return tmp;
}

StringValue& StringValue::operator+=(StringValue const &rhs) {
	size_t new_len = this->len_ + rhs.len_;
	char *new_data = new char[new_len + 1];
	strcpy(new_data, this->data_);
	strcat(new_data,rhs.data_);
	delete[] this->data_;
	this->len_ = new_len;
	this->data_ = new_data;
	return *this;
}

StringValue StringValue::operator+(const char *c) {
	StringValue tmp;
	delete[] tmp.data_;
	tmp.len_ = this->len_ + 1;
	tmp.data_ = new char[tmp.len_ + 1];
	strcpy(tmp.data_, this->data_);
	strcat(tmp.data_, c);
	return tmp;
}

StringValue& StringValue::operator+=(const char *c) {
	size_t new_len = this->len_ + 1;
	char *new_data = new char[new_len + 1];
	strcpy(new_data, this->data_);
	strcat(new_data, c);
	delete[] this->data_;
	this->len_ = new_len;
	this->data_ = new_data;
	return *this;
}

void StringValue::increaseRefCounter() {
	++ref_counter_;
}

void StringValue::decreaseRefCounter() {
	--ref_counter_;
}

int StringValue::get_ref_counter() {
	return ref_counter_;
}

void StringValue::printString(std::ostream &os) {
	for (size_t i = 0; i < len_; ++i) 
		os << data_[i];
}

int StringValue::get_len() {
	return len_;
}
	
	
	
MyString::MyString(char const *init) {
	str_value_ = new StringValue(init);
	//std::cout << "ctor " << init << std::endl; 
}

MyString::~MyString() {
	if(str_value_ != nullptr){
		str_value_->decreaseRefCounter();
		if(str_value_->get_ref_counter() == 0) {
			delete str_value_;
		}
	}
	//std::cout << "dtor" << std::endl;
}

MyString::MyString(MyString const &other) {
	str_value_ = other.str_value_;
	str_value_->increaseRefCounter();
	//std::cout << "copy ctor" << std::endl;
}

MyString::MyString(MyString &&other) noexcept{
	str_value_ = other.str_value_;
	other.str_value_ = nullptr;
	//std::cout << "move ctor" << std::endl;
}

MyString& MyString::operator=(MyString const &other) {
	if (this != &other) {
		str_value_->decreaseRefCounter();
		if(str_value_->get_ref_counter() == 0) {
			delete str_value_;
		}
		str_value_ = other.str_value_;
		str_value_->increaseRefCounter();
	}
	//std::cout << "ertekado op" << std::endl;
	return *this;
}

MyString& MyString::operator=(MyString &&other) noexcept {
	if (this != &other) {
		str_value_->decreaseRefCounter();
		if(str_value_->get_ref_counter() == 0) {
			delete str_value_;
		}
		str_value_ = other.str_value_; //move nem szuks
		other.str_value_ = nullptr;
		//str_value_->increaseRefCounter();
	}
	//std::cout << "move assignment" << std::endl;
	return *this;
}

MyString MyString::operator+(MyString const &rhs) const {
	MyString tmp;
	delete tmp.str_value_;
	tmp.str_value_ = new StringValue((*str_value_) + (*rhs.str_value_));
	return tmp;
}

MyString& MyString::operator+=(MyString const &rhs) {
	(*str_value_)+=(*rhs.str_value_);
	return *this;
}

MyString MyString::operator+(const char *c) {
	MyString tmp;
	delete tmp.str_value_;
	tmp.str_value_ = new StringValue((*str_value_) + c);
	return tmp;
}

MyString& MyString::operator+=(const char *c) {
	(*str_value_)+=(c);
	return *this;
}
	
char const& MyString::operator[](size_t index) const {
	//std::cout << "const []" << std::endl;
	return (*str_value_)[index];
}
	
char& MyString::operator[](size_t index) {
	//std::cout << "[]" << std::endl;
	//copy-on-write
	if (str_value_->get_ref_counter() > 1){
		str_value_->decreaseRefCounter();
		str_value_ = new StringValue(*str_value_);
	}
	//ha csak egydul hasznalja akkor siman csak megvaltoztathatja
	//olyan nem lehet h a ref count 0, mert ha egyedul hasznalja nem csokkenti, ha meg tobben akkor min 1re csokkenhet
	return (*str_value_)[index];
}

int MyString::get_len() {
	return str_value_->get_len();
}

void MyString::print(std::ostream &os) const {
	str_value_->printString(os);
}

int MyString::get_ref_counter() {
	return str_value_->get_ref_counter();
}

std::ostream& operator<<(std::ostream &os, MyString const &s) {
	s.MyString::print(os);
	return os;
}

std::istream& operator>>(std::istream &is, MyString &s) {
	char* arr = new char[100];
	int ctr = 0;
	char ch;
	int i = 1;
	is.get(ch);
	while(ch != '\n') {
		if (ctr == i * 100) {
			char *new_arr = new char[++i * 100];
			strcpy(new_arr, arr);
			delete[] arr;
			arr = new_arr;
		}
		arr[ctr++] = ch;
		is.get(ch);
	}
	std::cout << arr << std::endl;
	
	s = MyString(arr);
	delete[] arr;
	return is;
}
