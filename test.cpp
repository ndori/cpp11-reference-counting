#include <iostream>
#include <cstring>
#include <utility>

#include "mystring.h"

int main() {

	MyString a,b,c;
	a = b = c = "hello";
	std::cout << a << std::endl;
	
	MyString d = "alma";
	std::cout << d << std::endl;
	
	MyString e = d;
	std::cout << e << std::endl;
	
	MyString f = MyString("alma");
	std::cout << f << std::endl;
	
	a = d;
	std::cout << a << std::endl;
	
	a = MyString("fa");
	std::cout << a << std::endl;
	
	MyString g = f + a;
	std::cout << g << std::endl;
	
	f += a;
	std::cout << f << std::endl;
	
	std::cout << b[0] << std::endl;
	
	b[0] = 'H';
	std::cout << b << std::endl;
	std::cout << c << std::endl;
	
	MyString h = "korte";
	h[0] = 'K';
	std::cout << h << std::endl;
	
	std::cout << d.get_len() << std::endl;

	return 0;
}
