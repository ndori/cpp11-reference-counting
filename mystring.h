#ifndef MYSTRING_H_INCLUDED
#define MYSTRING_H_INCLUDED

class StringValue {
private:
	int ref_counter_;
	size_t len_;
	char *data_;
	
public:
	StringValue(char const *init="");
	
	~StringValue();
	
	StringValue(StringValue const &other);
	
	StringValue(StringValue &&other) noexcept;
	
	StringValue& operator=(StringValue const &other);
	
	StringValue& operator=(StringValue &&other) noexcept;
	
	char const& operator[](size_t index) const;
	
	char& operator[](size_t index);
	
	StringValue operator+(StringValue const &rhs) const;
	
	StringValue& operator+=(StringValue const &rhs);
	
	StringValue operator+(const char *c);
	
	StringValue& operator+=(const char *c);
	
	void increaseRefCounter();
	
	void decreaseRefCounter();
	
	int get_ref_counter();
	
	void printString(std::ostream &os);
	
	int get_len();
	
};

class MyString {
private:
	StringValue *str_value_;
	
public:
	MyString(char const *init="");
	
	~MyString();
	
	MyString(MyString const &other);
	
	MyString(MyString &&other) noexcept;
	
	MyString& operator=(MyString const &other);
	
	MyString& operator=(MyString &&other) noexcept;
	
	MyString operator+(MyString const &rhs) const;
	
	MyString& operator+=(MyString const &rhs);
	
	MyString operator+(const char *c);
	
	MyString& operator+=(const char *c);
	
	char const& operator[](size_t index) const;
	
	char& operator[](size_t index);
	
	int get_len();
	
	void print(std::ostream &os) const;
	
	int get_ref_counter();
	
};

std::ostream& operator<<(std::ostream &os, MyString const &s);


std::istream& operator>>(std::istream &is, MyString &s);

#endif
